<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutopartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autoparts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('partsname');
            $table->boolean('used');
            $table->string('firm');
            $table->string('model');
            $table->integer('kuzov');
            $table->integer('engine');
            $table->integer('modelnumber');
            $table->char('R_L',1);
            $table->char('U_D',1);
            $table->char('F_R',1);
            $table->string('oem_code');
            $table->string('producer');
            $table->string('producer_code');
            $table->float('price');
            $table->string('currency');
            $table->text('info');
            $table->string('s_presence');
            $table->string('photo');
            $table->string('video');
            $table->string('link');
            $table->integer('origcode');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('autoparts');
    }
}
