@extends('layouts.base')

@section('title')
    Автозапчасти

@stop

@include('layouts.header')

@section('content')

    @include('errors.validate_errors')
    @include('layouts.form')


@stop