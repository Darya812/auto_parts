<script language="JavaScript" src="/baljukevich/AutoParts/resources/js/check_words.js" ></script>

<div id="token" hidden="hidden">{{Session::getToken()}}</div>
{!! Form::open(array('method'=>'post','url'=>'info', 'class' => 'form-horizontal')) !!}
<div id="partsname" class="form-group">
    {!! Form::label('partsname', 'Название запчасти', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('partsname', null, array('class' => 'form-control', 'placeholder' => 'Название запчасти'))!!}
    </div>

</div>

<div id="used" class="form-group">
    {!! Form::label('used', 'Состояние запчасти:', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!! Form::select('used',array('0' => 'Новая', '1' => 'Контрактная'), '0',array('class'=>'form-control')) !!}
    </div>

</div>

<div id="firm" class="form-group">
    {!! Form::label('firm', 'Марка автомобиля', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('firm', null, array('class' => 'form-control', 'placeholder' => 'Марка автомобиля'))!!}
    </div>

</div>

<div id="model" class="form-group">
    {!! Form::label('model', 'Модель автомобиля', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('model', null, array('class' => 'form-control', 'placeholder' => 'Модель автомобиля'))!!}
    </div>

</div>

<div id="kuzov" class="form-group">
    {!! Form::label('kuzov', 'Номер кузова', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('kuzov', null, array('class' => 'form-control', 'placeholder' => 'Номер кузова'))!!}
    </div>

</div>

<div id="engine" class="form-group">
    {!! Form::label('engine', 'Номер двигателя', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('engine', null, array('class' => 'form-control', 'placeholder' => 'Номер двигателя'))!!}
    </div>

</div>

<div id="modelnumber" class="form-group">
    {!! Form::label('modelnumber', 'Название оптики,двс/артикул запчасти', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('modelnumber', null, array('class' => 'form-control', 'placeholder' => 'Название запчасти'))!!}
    </div>

</div>

<div id="R_L" class="form-group">
    {!! Form::label('R_L', 'Местоположение запчасти', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!! Form::select('R_L',array('R' => 'Право', 'L' => 'Лево','-'=>'-'), '-',array('class'=>'form-control')) !!}
    </div>

</div>


<div id="U_D" class="form-group">
    {!! Form::label('U_D', 'Местоположение запчасти', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!! Form::select('U_D',array('U' => 'Верх', 'D' => 'Низ','-'=>'-'), '-',array('class'=>'form-control')) !!}
    </div>

</div>

<div class=""></div>
<div id="F_R" class="form-group">
    {!! Form::label('F_R', 'Местоположение запчасти', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!! Form::select('F_R',array('F' => 'Перед', 'R' => 'Зад','-'=>'-'), '-',array('class'=>'form-control')) !!}
    </div>

</div>

<div id="oem_code" class="form-group">
    {!! Form::label('oem_code', 'OEM код', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('oem_code', null, array('class' => 'form-control', 'placeholder' => 'ОЕМ код'))!!}
    </div>

</div>

<div id="producer" class="form-group">
    {!! Form::label('producer', 'Производитель запчасти', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('producer', null, array('class' => 'form-control', 'placeholder' => 'Производитель запчасти'))!!}
    </div>

</div>


<div id="producer_сode" class="form-group">
    {!! Form::label('producer_сode', 'Код производителя', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('producer_сode', null, array('class' => 'form-control', 'placeholder' => 'Код производителя'))!!}
    </div>

</div>


<div id="price" class="form-group">
    {!! Form::label('price', 'Цена', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('price', null, array('class' => 'form-control', 'placeholder' => 'Цена'))!!}
    </div>

</div>


<div id="currency" class="form-group">
    {!! Form::label('currency', 'Валюта', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('currency', null, array('class' => 'form-control', 'placeholder' => 'Валюта'))!!}
    </div>

</div>


<div id="info" class="form-group">
    {!! Form::label('info', 'Дополнительная информация', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::textarea('info', null, array('class' => 'form-control', 'placeholder' => 'Дополнительная информация'))!!}
    </div>

</div>

<div id="s_presence" class="form-group">
    {!! Form::label('s_presence', 'Наличие', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!! Form::select('s_presence',array('availability' => 'В наличии', 'custom' => 'Под заказ','on_the_way'=>'в пути'), 'availability',array('class'=>'form-control')) !!}
    </div>

</div>


<div id="photo" class="form-group">
    {!! Form::label('photo', 'Фото', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('photo', null, array('class' => 'form-control', 'placeholder' => 'Вставьте ссылки на фото'))!!}
    </div>

</div>


<div id="video" class="form-group">
    {!! Form::label('video', 'Видео', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('video', null, array('class' => 'form-control', 'placeholder' => 'Вставьте ссылку на видео'))!!}
    </div>

</div>


<div id="link" class="form-group">
    {!! Form::label('link', 'Ссылка на информацию об запчасти', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('link', null, array('class' => 'form-control', 'placeholder' => 'Ссылка на информацию об запчасти'))!!}
    </div>

</div>


<div id="origcode" class="form-group">
    {!! Form::label('origcode', 'Код для заказа', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('origcode', null, array('class' => 'form-control', 'placeholder' => 'Ваш внутренний код товара'))!!}
    </div>

</div>



<div class="form-group">
    {!! Form::button('Сохранить', array('class' => 'btn btn-default center-block', 'name'=>'get_data','type' => 'submit', 'onclick'=>'getData()')) !!}
    <span id="result"></span>
</div>

{!! Form::close() !!}
