<div class="form-group">
    {!! Form::label('translation', 'Translation', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div id="translation" class="col-md-6">
        <p class="form-control-static"></p>
    </div>
</div>

<div id="infinitive" class="form-group">
    {!! Form::label('infinitive', 'Infinitive', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('infinitive', null, array('class' => 'form-control', 'placeholder' => 'Infinitive','oninput'=>'isInfinitive()'))!!}
        <span id="result"></span>
    </div>
</div>

<div id="past" class="form-group">
    {!! Form::label('past', 'Past', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('past', null, array('class' => 'form-control', 'placeholder' => 'Past','oninput'=>'isPast()'))!!}
        <span id="result1"></span>
    </div>
</div>

<div id="past_participle" class="form-group">
    {!! Form::label('past_participle', 'Past participle', array(
    'class' =>'col-md-3 control-label'
    )) !!}
    <div class="col-md-6">
        {!!Form::text('past_participle', null, array('class' => 'form-control', 'placeholder' => 'Past participle','oninput'=>'isPastParticiple()'))!!}
        <span id="result2"></span>
    </div>
</div>