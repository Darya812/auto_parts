<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class XmlController extends Controller
{


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $xml = new \DomDocument('1.0', 'utf-8');
        $sorts = $xml->appendChild($xml->createElement('sorts'));
        $sort = $sorts->appendChild($xml->createElement('sort'));
        $name = $sort->appendChild($xml->createElement('name'));
        $name->appendChild($xml->createTextNode('Яблоко'));
        $xml->formatOutput = true;
        echo $xml->saveXML();
    }
}

