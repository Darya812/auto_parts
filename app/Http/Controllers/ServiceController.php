<?php

namespace App\Http\Controllers;

use App\AutoParts;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getInfo()
    {
        return view('home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Requests\AutoPartsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postInfo(Requests\AutoPartsRequest $request)
    {
        AutoParts::create($request->all());
        return view('sucsess');

    }


}
