<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutoParts extends Model
{
    protected $table = 'autoparts';

    protected $fillable = [
        'partsname',
        'used',
        'firm',
        'model',
        'kuzov',
        'engine',
        'modelnumber',
        'R_L',
        'U_D',
        'F_R',
        'oem_code',
        'producer',
        'producer_code',
        'price',
        'currency',
        'info',
        's_presence',
        'photo',
        'video',
        'link',
        'origcode'
    ];
}
